"""
Clase CodonUsage

Objeto que contiene la informacion del uso de codones de un organismo.
Los conteos se guardan en diccionarios, se tienen para genes nucleares, cloroplasticos y mitocondriales.

Tambien se puede contar la cantidad de codones totales contados por diccionario,
ademas de determinar los diccionarios de uso de manera proporcional por cada 1000 codones.

Existen metodos para poder mostrar la informacion de manera ordenada, ademas de un metodo para poder
obtener la secuencia nucleotidica necesaria para poder traducir a cierta proteina, a partir de un
diccionario generado que indica el codon preferido para cada aminoacido (Busqueda de secuencia optimizada).

"""

import matplotlib.pyplot as plt
import matplotlib.cm as cm

class CodonUsage:
    def __init__(self):
        # Constructor, atributos a usar
        # Diccionarios de uso, cada aminoacido con sus codones, y los codones con su numero de usos
        self._nuc_codon_usage = {}
        self._mit_codon_usage = {}
        self._chl_codon_usage = {}
        self._usages = [self._nuc_codon_usage, self._mit_codon_usage, self._chl_codon_usage]

        # Cantidad total de codones de cada diccionario de uso
        self._nuc_total = 0
        self._mit_total = 0
        self._chl_total = 0
        self._totals = [self._nuc_total, self._mit_total, self._chl_total]

        # Diccionario de preferencias, cada aminoacido con el codon preferido
        self._nuc_preferences = {}
        self._mit_preferences = {}
        self._chl_preferences = {}
        self._preferences = [self._nuc_preferences, self._mit_preferences, self._chl_preferences]

        # Diccionario de uso proporcional, uso de codon por cada 1000 usos
        self._nuc_proportions = {}
        self._mit_proportions = {}
        self._chl_proportions = {}
        self._proportions = [self._nuc_proportions, self._mit_proportions, self._chl_proportions]

        self._names = ["nucleares", "mitocondriales", "cloroplásticos"]
        

    # Getters de los diccionarios de uso
    @property
    def nuc_usage(self):
        return self._nuc_codon_usage

    @property
    def mit_usage(self):
        return self._mit_codon_usage

    @property
    def chl_usage(self):
        return self._chl_codon_usage

    # Agrega informacion a un diccionario de uso, numero absoluto
    def add(self, aminoacid, codon, name):
        # Condiciona a que diccionario agregar dependiendo del valor de la variable "name"
        if "mitochondrion" in name:
            collection = self._mit_codon_usage
        elif "chloroplast" in name:
            collection = self._chl_codon_usage
        elif not "X" in name or not "Y" in name:
            collection = self._nuc_codon_usage
        
        # Si no se encuentra alguna key, se agrega y se cuenta, si no solo se aumenta el valor previo
        if aminoacid in collection.keys():
            if codon in collection[aminoacid].keys():
                collection[aminoacid][codon] += 1
            else:
                collection[aminoacid][codon] = 1
        else:
            collection[aminoacid] = {}
            collection[aminoacid][codon] = 1    

    # Getters para el total de codones de cada diccionario
    @property
    def nuc_total(self):
        return self._nuc_total

    @property
    def mit_total(self):
        return self._mit_total

    @property
    def chl_total(self):
        return self._chl_total

    # Calcula la cantidad total de codones en cada diccionario
    def set_total_codons(self, collection):
        total = 0 # Calcular la cantidad total de codones que se tiene
        for aminoacid in collection.keys():
            total += sum(collection[aminoacid].values())
        
        return total

    # Getters para el diccionario con las preferencias
    @property
    def nuc_pref(self):
        return self._nuc_preferences

    @property
    def mit_pref(self):
        return self._mit_preferences

    @property
    def chl_pref(self):
        return self._chl_preferences

    # Determina las preferencias de uso de codon
    def set_preferences(self, collection, preferences):
        for aminoacid in collection.keys():
            for codon in collection[aminoacid].keys():
                if collection[aminoacid][codon] == max(collection[aminoacid].values()):
                    preferences[aminoacid] = codon
                    break
    
    # Getters para los diccionarios con las proporciones de uso
    @property
    def nuc_prop(self):
        return self._nuc_proportions

    @property
    def mit_prop(self):
        return self._mit_proportions

    @property
    def chl_prop(self):
        return self._chl_proportions

    # Determina las proporciones de uso, por cada 1000
    def set_proportions(self, collection, target, total):
        # No se puede dividir por 0
        if total == 0:
            return
        # Ir agregando informacion al diccionario objetivo
        for aa in collection.keys():
            target[aa] = {}
            for codon in collection[aa].keys():
                target[aa][codon] = (collection[aa][codon]/total) * 1000
            
    
    # Mostrar la informacion de un diccionario de uso, ya sea proporciones o absoluto
    def display_usage(self, collection):
        text = ""
        final_text = ""
        for aa in collection.keys():
            text += f"\n{aa}: "
            for codon in collection[aa].keys():
                text += f"{codon}: {collection[aa][codon]} "
        text = text + "\n" 
        print(text)

    # Muestra de manera ordenada el diccionario de preferencias de codones
    def display_pref(self, collection):
        i = 0
        text = ""
        for aa in collection.keys():
            if i < 3:
                text += f"{aa}: {collection[aa]}\t"
            else:
                text += f"\n{aa}:{collection[aa]}\t"
                i = 0
            i += 1
        text = f"\n{text}\n"
        print(text)


    # Recibe una secuencia aminoadica, busca la secuencia nucleotidica
    # en funcion de las preferencias del organismo
    def get_preferred_sequence(self, amino_seq, preferences):
        nuc_seq = ""
        for aa in amino_seq:
            nuc_seq += preferences[aa]
        return nuc_seq
        
    # Se dan valor a las preferencias y a los valores total
    # Se otorga valor a los atributos previamente no usados
    def set_values(self):
        
        # Total de cada cosa
        self._nuc_total = self.set_total_codons(self._nuc_codon_usage)
        self._mit_total = self.set_total_codons(self._mit_codon_usage)
        self._chl_total = self.set_total_codons(self._chl_codon_usage)

        # Preferencias
        self.set_preferences(self._nuc_codon_usage, self._nuc_preferences)
        self.set_preferences(self._mit_codon_usage, self._mit_preferences)
        self.set_preferences(self._chl_codon_usage, self._chl_preferences)

        # Proporciones
        self.set_proportions(self._nuc_codon_usage,
                             self._nuc_proportions, self._nuc_total)
        self.set_proportions(self._mit_codon_usage,
                             self._mit_proportions, self._mit_total)
        self.set_proportions(self._chl_codon_usage,
                             self._chl_proportions, self._chl_total)
        self._totals = [self._nuc_total, self._mit_total, self._chl_total]


    # Graficar el uso absoluto
    def plot_usages(self, organism):
        for i in range(len(self._names)):
            # Genero y especie 
            name_parts = organism.split(" ")
            genus = name_parts[0]
            species = name_parts[1]

            # Determinar listas de informacion para el grafico
            amino_acids = []
            codons = []
            counts = []
            usage = self._usages[i]
            for aa in usage.keys():
                amino_acids.append(aa)
                codons.append(list(usage[aa].keys()))
                counts.append(list(usage[aa].values()))

            # Mapa de colores
            cmap = cm.get_cmap("gist_ncar", 74) # Mapa de colores a usar, 80 colores
            colormap = {}
            
            num = 10 # Partir del color numero 10, por tema de contraste con el fondo
            # Construir mapa en diccionario codon:color
            for minilist in codons:
                for codon in minilist:
                    colormap[codon] = cmap(num)
                    num += 1
            # Crear subplots
            fig, ax = plt.subplots(figsize=(16, 14))

            # Iterate over each amino acid
            for j, aa in enumerate(amino_acids):
                y_start = 0 # Piso por el que parte cada aminoácido
    
                # Recorrer el conteo de codones en cada aminoacido
                for k, count in enumerate(counts[j]):
                    height = count
                    # Barra a agregar del codon en cuestión, con su color correspondiente
                    ax.bar(j, height, bottom=y_start, color = colormap[codons[j][k]])
                    # Colocar en cada barra a cual codon corresponde
                    ax.text(j, y_start + height/2, codons[j][k], ha = "center", va = "center", fontsize = 10, color = "black")
                    
                    # Actualizar el valor de inicio para la siguiente barra (para colocar cada una sobre la otra)
                    y_start += height
                    
            # Etiquetas para el grafico
            ax.set_xlabel("Aminoácido")
            ax.set_ylabel("Conteo")
            ax.set_title(f"Uso de codones para genes {self._names[i]} en $\it{genus}$ $\it{species}$")
            ax.set_xticks(range(len(amino_acids)))
            ax.set_xticklabels(amino_acids)

            
            plt.tight_layout()
            plt.savefig(self._names[i] + "_" + organism + ".png")
            #plt.show()
            plt.close()
            
    # Guardar la tabla de preferencias de uso de codones
    def save_preferences(self, organism):
        text = ""
        i = 0
        for pref in self._preferences:
            for key in pref.keys():
                text += f"{key}\t{pref[key]}\n"
            name = f"{self._names[i]}_preferences_{organism}"
            with open(name, "w") as f:
                f.write(text)
            i += 1;
            text = ""
            
            
    # Mostrar la informacion de cada uno de los usos
    def print_all(self):
        for i in range(len(self._usages)):
            print(self._names[i])
            print("Uso total")
            self.display_usage(self._usages[i])
            print("Total de codones")
            print(self._totals[i])
            print("Preferencias")
            self.display_pref(self._preferences[i])
            print("Proporciones")
            self.display_usage(self._proportions[i])
            print("=" * 80)      

