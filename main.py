"""
Proyecto 2 - Sebastián Bustamante Villarreal

A partir de la información contenida dentro del archivo gbff de un organismo, se cuenta cuantas veces se usa
cada codon, ya sea de manera completa en el genoma, en genes mitocondriales, cloroplasticos o nucleares.

Luego del conteo se determinan las preferencias de uso de codon del organismo, tomando como codon valido
aquel codon que fue utilizado la mayor cantidad de veces, esto se hace por separado para todos los
diccionarios creados para el conteo.

Luego de determinar las preferencias, a partir de una secuencia nucleotidica dada se tiene una
herramienta que puede genrar la secuencia nucleotidica sinonima, utilizando
los codones de preferencia para cada aminoacido
(esto usando el diccionario nuclear, cloroplastico, mitocondrial o el que se desee).

Importante: Para que el programa funcione se debe tener el archivo de anotacion (gbff) del organismo a estudiar,
tambien definir el nombre cientifico del organismo, para los graficos que se generen.
"""

# Modulos a utilizar
import threading
from queue import Queue
from Bio import SeqIO
from Bio.Seq import Seq
from codon_usage import CodonUsage

# Capacidad del buffer, mutex, buffer y cantidad de consumidores
bf_size = 100
mtx = threading.Lock()
buffer = Queue(maxsize = bf_size)
nc = 10

# Objeto CodonUsage que funcionara como variable global
usage = CodonUsage()


# Se toma una secuencia nucleotidica y se separa en codones, a partir del nucleotido "start"
def split_codons(seq, start):
    codon = ""
    codons = []
    seq = seq[start:]
    for nuc in seq:
        codon += nuc
        if len(codon) == 3:
            codons.append(codon)
            codon = ""
    return codons


# Procesa un cromosoma, coloca el par translation y secuencia nucleotidica en el buffer
def process_chr(chr):
    for feature in chr.features:
        if feature.type == "CDS":
            # Pasar la secuencia y la translation al buffer
            if not "translation" in feature.qualifiers.keys():
                continue
            # Se agrega tambien el nombre del cromosoma
            info = {"nseq":feature.extract(chr).seq, "aseq":feature.qualifiers["translation"][0]+"*",
                    "start":int(feature.qualifiers["codon_start"][0]), "name":chr.description}
            buffer.put(info)
    

# Saca un par (cromosoma - secuencia) del buffer, cuenta cuantas veces se usa cada codon
def process_pair():
    # Se consume de manera indefinida, hasta que no queden mas elmentos y los consumidores hayan terminado
    while True:
        try:
            info = buffer.get(timeout=3)
        except:
            break
        buffer.task_done() # Notificar que se eliminó correctamente del buffer
        mtx.acquire() # Bloquear antes de modificar el objeto CodonUsage
        codons = split_codons(info["nseq"], info["start"]-1) # Obtener los codones de la secuencia
        for i, codon in enumerate(codons): # Contar codon a codon
        # Se condiciona donde se agrega la informacion dependiendo del nombre del cromosoma
            usage.add(info["aseq"][i], codon, info["name"]) # agrega donde corresponda
        mtx.release() # Liberar luego de terminar de modificar al objeto


def main():

    # Cada record corresponde a un cromosoma
    # Lista para productores y consumidores
    producers = []
    consumers = []

    # Archivo de anotacion y especie de estudio
    filename = "carsonella_ruddii.gbff"
    esp_name = "Carsonella rudii"
    
    # Los productores, tantos como la cantidad de cromosomas
    with open(filename) as handle:
        for chr in SeqIO.parse(handle, "genbank"):
            thread = threading.Thread(target=process_chr, args=[chr])
            producers.append(thread)
            thread.start()
            
        
    # Los consumidores (serán nc de manera fija)
    for i in range(nc):
        thread = threading.Thread(target=process_pair)
        consumers.append(thread)
        thread.start()

    # Esperar a que terminen los productores y consumidores
    for producer in producers:
        producer.join()
    for consumer in consumers:
        consumer.join()

    # Objeto Usage listo para manipular
    usage.set_values() # Importante inicializar los valores luego de contar los codones
    usage.print_all()
    usage.plot_usages(esp_name)
    usage.save_preferences(esp_name)

    # Ejemplo de buscar una secuencia nucleotidica optimizada a partir de una secuencia nucleotidicia original, secuencia inventada
    nseq = Seq("ATGGTAGTGATGATGTGATGATGATAGTAGATGAGTCGATCGTCGATCTGTCAGCTA")
    print("Secuencia optimizada:", usage.get_preferred_sequence(nseq.translate(), usage.nuc_pref)) 

    
if __name__ == "__main__":
    main()
