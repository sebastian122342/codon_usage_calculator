# Solucion lineal sin productor consumidor al problema

# Proyecto 2 - Sebastián Bustamante

# se usan los archivos de anotacion de un organismo procariota unicelular
# para saber cual e su uso preferencial de codones
# saber cual es el uso preferencial de codones de un organismo es importante pues,
# si se ingresa una cierta secuencia nucleotidica que en el humano traduce a la proteina X en una
# bacteria, por el mero hecho de la preferencia de uso de codones, es posible que esta secuencia no
# genere el mismo producto.
# Por lo tanto la secuencia nucleotidica se tendria que re-rescribir segun el uso de codones de ese organismo
# para podrer ahí recién ingresarlo y que traduzca a lo que se debe

# es importante saber esto pues hay algunas aplicaciones medicas y biotecnológicas en las que se ingresan genes
# humanos dentro de otros organimos para poder aprovechar su maquionaria y sinstetizar ciertas proteínas en
# grandes cantidades, como lo es el caso de la insulina.

# Como se calculara este sesgo de codones.
# Se trabajara con un organismo procariota.
# Se puede probar primero con carsonella ruddi.

# id es el cromosoma (cada record es un cromosoma)
   # con chr.seq se puede acceder a la secuencia del cromosoma
   # las features del cromosoma son los tags de la izquierda,
   # si es que es CDS, gene, source, etc
   # los qualifiers de una feature corresponden a la parte de mas de la derecha,
   # los tags que tienen un / al inicio
   # creo que tambien se puede sacar la location de una feature
   # feature.location da el conjunto entero
   # feature.location.start da el inicio
   # feature.location.end da el final
   # feature.location.strand 
   #for chr in chromosomes:
    #   for feature in chr.features:
            # print(feature) # DE AQUI SE PUEDE SACAR MUCHA INFO
     #       if feature.type == "CDS":
                #pass
                # print(feature.qualifiers["translation"])
                # print(feature.location.start)
                # print(feature.location.end)
                # print(feature.location.strand)
      #          print("\n", feature.extract(chr).seq) # saca la secuencia de la CDS

from codon_usage import CodonUsage
from Bio import SeqIO

def split_codons(seq, start):
    codon = ""
    codons = []
    seq = seq[start:]
    for nuc in seq:
        codon += nuc
        if len(codon) == 3:
            codons.append(codon)
            codon = ""
    return codons 

def main():
    usage = CodonUsage()

    chromosomes = []
    with open("yeast.gbff") as handle:
        for record in SeqIO.parse(handle, "genbank"):
            chromosomes.append(record)
    for chr in chromosomes:
        print(f"{chr.description}") # EL NOMBRE Y NUMERO DEL CROMOSOMA
        for feature in chr.features:
            print(f"HOLA {chr.features}")
            if feature.type == "CDS":
                if not "translation" in feature.qualifiers.keys():
                    continue
                #break
                info = {"nseq":feature.extract(chr).seq, "aseq":feature.qualifiers["translation"][0]+"*",
                        "start":int(feature.qualifiers["codon_start"][0])}
                codons = split_codons(info["nseq"], info["start"]-1)
                for i in range(len(info["aseq"])):
                    usage.add(info["aseq"][i], codons[i])
    #print(usage.usage)
    
   

if __name__ == "__main__":
    main()

"""

    # Graficar el uso de codones
    def plot_usages(self, organism):
        for i in range(len(self._names)):
            temp_dict = {}

            name_parts = organism.split(" ")
            genus = name_parts[0]
            species = name_parts[1]
            
            for aa in self._usages[i]:
                temp_dict[aa] = sum(self._usages[i][aa].values())

            aas = list(temp_dict.keys())
            counts = list(temp_dict.values())

            plt.bar(aas, counts, color = "maroon", width = 0.4)
            
            plt.xlabel("Aminoácido")
            plt.ylabel("Conteo de codones")
            plt.title(f"Uso de codones - {self._names[i]} en $\it{genus}$ $\it{species}$")

            plt.tight_layout()

            plt.savefig(self._names[i] + "_" + organism + ".png")
            #plt.legend()
            plt.close()

def better_plot(self, organism):
        for i in range(len(self._names)):
            name_parts = organism.split(" ")
            genus = name_parts[0]
            species = name_parts[1]

            amino_acids = []
            codons = []
            counts = []
            usage = self._usages[i]
            for aa in usage.keys():
                amino_acids.append(aa)
                codons.append(list(usage[aa].keys()))
                counts.append(list(usage[aa].values()))

            # Mapa de colores
            cmap = cm.get_cmap("gist_ncar", 74) # Mapa de colores a usar, 80 colores
            colormap = {}
            
            num = 10 # Partir del color numero 10, por tema de contraste con el fondo
            # Construir mapa en diccionario codon:color
            for minilist in codons:
                for codon in minilist:
                    colormap[codon] = cmap(num)
                    num += 1
            # Crear subplots
            fig, ax = plt.subplots(figsize=(16, 14))

            # Iterate over each amino acid
            for j, aa in enumerate(amino_acids):
                y_start = 0 # Piso por el que parte cada aminoácido
    
                # Recorrer el conteo de codones en cada aminoacido
                for k, count in enumerate(counts[j]):
                    height = count
                    # Barra a agregar del codon en cuestión, con su color correspondiente
                    ax.bar(j, height, bottom=y_start, color = colormap[codons[j][k]])
                    # Colocar en cada barra a cual codon corresponde
                    ax.text(j, y_start + height/2, codons[j][k], ha = "center", va = "center", fontsize = 10, color = "black")
                    
                    # Actualizar el valor de inicio para la siguiente barra (para colocar cada una sobre la otra)
                    y_start += height
                    
            # Etiquetas para el grafico
            ax.set_xlabel("Aminoácido")
            ax.set_ylabel("Conteo")
            ax.set_title(f"Uso de codones para genes {self._names[i]} en $\it{genus}$ $\it{species}$")
            ax.set_xticks(range(len(amino_acids)))
            ax.set_xticklabels(amino_acids)

            
            plt.tight_layout()
            plt.savefig(self._names[i] + "_" + organism + ".png")
            #plt.show()
            plt.close()


"""
